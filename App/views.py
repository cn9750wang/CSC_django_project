import json
import uuid

from django.core import serializers
from django.http import HttpResponse
from django.shortcuts import render,redirect

# Create your views here.
# 登陆


# 登陆
from App import models

# 用户登录检查装饰器
def is_login(callback):
    def check_admin(request,*callback_args, **callback_kwargs):
        # 判断是不是管理登陆
        if not request.session.get('uid'):
            return redirect('/login')
        return callback(request,*callback_args,**callback_kwargs)
    return check_admin

# 设置公共数据
def get_common_data(request):
    response = {}
    # 登陆的uid装进response
    if request.session.get('uid', False):
        response['uid'] = request.session['uid']
    # 把所有类别放入返回字典
    kinds = models.Kind.objects.filter()
    response['kinds'] = kinds

    return response

# 登录
def login(request):
    if request.method == 'GET':
        return render(request,'login.html')
    elif request.method == 'POST':
    #     验证输入的用户名和密码是否正确，然后存入session
        type = request.POST.get('type')
        print(type)
        response = {'msg':'','status':False}
        # 获取前端的数据
        uid = request.POST.get('uid')
        pwd = request.POST.get('pwd')
        # 统一在登陆方法中判断这个输入输出框按钮是哪种状态
        if type == 'login':
        #     登陆验证  判断用户名和密码
            if len(models.User.objects.filter(uid=uid,password=pwd))!=0:
        #         登陆成功  -----异步实现的
                print(uid,pwd)
                response['status'] = True
                request.session['uid'] = uid
                return HttpResponse(json.dumps(response))
            else:
        #         登陆失败的情况
                response['msg'] = '用户名或者密码错误'
                return HttpResponse(json.dumps(response))
        elif type=='register':
            models.User.objects.create(uid=uid,password=pwd)
            response['status']=True
            request.session['uid']=uid
            return HttpResponse(json.dumps(response))
        else:
            response['msg'] ='该用户已经被注册'
            return HttpResponse(json.dumps(response))



# 不是所有的方法都需要在路由立马添加
# 注册的方式
def register(request):
    # 请求方式  post  如果是请求文件上传  都是使用post
    if request.method == 'POST':
        uid = request.POST.get('uid')
        pwd = request.POST.get('pwd')
    #     验证这个用户是否被注册
        if len(models.User.objects.filter(uid=uid))!=0:
            #         已经被创建  返回错误提示
            return render(request,'login.html',{'msg':'该用户已经被注册'})
        else:
    #         插入---增加
            user = {
                'uid':uid,
                'password':pwd,
            }
            models.User.objects.create(**user)
            return render(request, 'home.html')
# 运行方式：工具自带（）  通过终端命令运行（自动热部署）

# 首页
def home(request):
    if request.method == 'GET':
        # 页面响应的结果集
        response = get_common_data(request)

        # top10公告处理，筛选出10个  查出10个
        announcements=models.Announcement.objects.filter().all()[:10]
        response['a_list'] = announcements

        # 最新帖子 topics
        topics = models.Topic.objects.filter()
        keys = request.GET.get('keys', '')
        if keys != '':
            # 按关键字查询标题里含有关键字的
            topics = topics.filter(t_title__icontains=keys)
            response['keys'] = keys
        # 取前100
        topics = topics.order_by('-id').all()[:100]
        response['topics'] = topics

        # 推荐帖子  利用是否推荐的字段,取前10
        recommends=models.Topic.objects.filter(recommend=True)[:10]
        response['r_list'] = recommends

        return render(request, 'home.html', response)

# 修改密码
def edit_pwd(request):
    # 一般修改密码都是在完成登陆之后进行  根据所登陆的uid去进行修改
    if request.method == 'GET':
        # 通过请求先获取session中的uid
        uid = request.session.get('uid')
        # 跳转页面
        return render(request,'edit-pwd.html',{'uid':uid})
    # 重定向  转发
    if request.method == 'POST':
        uid =request.session.get('uid')
        old = request.POST.get('old_pwd')
        new1=request.POST.get('new_pwd1')
        new2=request.POST.get('new_pwd2')
    #     逻辑判断
        if new1 == new2 and len(models.User.objects.filter(uid=uid,password=old))!=0:
    #         核对成功   将数据表更新
            models.User.objects.filter(uid=uid).update(password=new1)
        return redirect('/home')   #url路由
    """
    转发：一次请求和响应，请求的地址没有发生变化，如果此时刷新页面，就会出现重做现象。
    重定向：一次以上的请求和响应，请求地址发生一次以上的变化，如果此时刷新页面，
    就不会发生重做现象。
    
    修改页面  记得加上非空验证
    """

#发布帖子的实现
def publish(request):
    if request.method == 'GET':
        return render(request,'publish.html',get_common_data(request))
    elif request.method == 'POST':
    #     根据登陆的信息获取uid
        uid= request.session.get('uid')
        # 当点击提交时提交到数据的信息
        t_title=request.POST.get('t_title')
        t_introduce=request.POST.get('t_introduce')
        t_content=request.POST.get('t_content')
        t_kind = request.POST.get('t_kind')
        if not models.Kind.objects.filter(id=t_kind):
            # 错误,没有该板块
            return
        # 数据获取完成
        print(t_title,t_introduce,t_content,t_kind)

        obj = models.Topic.objects.create(t_title=t_title,
                 t_introduce= t_introduce,t_content=t_content,
                 t_kind=t_kind,t_uid=uid
                                          )
        t_id = obj.id
    #     进行图片处理
        t_photo = request.FILES.get('t_photo',None)
        if t_photo:
            t_photo_path = 'static/img/t_photo/' + str(t_id) + '_' + t_photo.name
            import os
            file = open(os.path.join(t_photo_path),'wb')
            for line in t_photo.chunks():
                file.write(line)
            file.close()
            #执行数据库的更新语句将数据存入
            models.Topic.objects.filter(id=t_id).update(t_photo='/'+t_photo_path)
        return redirect('/single/'+str(t_id))

# 显示单个帖子
def single(request,tid):
    if request.method =='GET':
    #     找帖子的信息  如果表中存在这个id  那么就显示  如果不存在返回到首页
        try:
            topic = models.Topic.objects.get(id=tid)
        except Exception as e:
            return redirect('/home')
        kind = models.Kind.objects.filter(id=topic.t_kind).get()
        if not kind:
            # 错误,没有该板块
            return
        # 帖子的信息
        t_time = topic.create_time
        t_kind = kind.k_name #实际为k_name但是为了最小限度改变代码，只能偷梁换柱
        t_content = topic.t_content
        t_uid = topic.t_uid
        t_title = topic.t_title
        t_photo = topic.t_photo
        t_introduce = topic.t_introduce
        # 9个信息
        response = {
            'tid':tid,
            't_uid':t_uid,
            't_time':t_time,
            't_kind':t_kind,
            't_title':t_title,
            't_content':t_content,
            't_photo':t_photo,
            't_introduce':t_introduce,
            # 对象解构
            **get_common_data(request)
        }
        # 留言内容的处理
        # 留言的时间  留言的内容 留言者
        replys = models.Reply.objects.filter(r_tid=tid)
        reply_list= []
        for reply in replys:
            single_reply={
                'r_uid':reply.r_uid,
                'r_content':reply.r_content,
                'r_time':reply.r_time,
                'r_photo':reply.r_photo,
                'r_id':reply.id,
            }
            reply_list.append(single_reply)
        response['reply_list'] = reply_list
        return render(request,'single.html',response)
    elif request.method == 'POST':
        # 是否登陆
        uid = request.session.get('uid')
        # 想进行删除  管理员
        if not uid:
            return redirect('/login')
        # 进行留言
        r_content = request.POST.get('r_content')
        # 提交到数据库
        obj = models.Reply.objects.create(r_tid=tid,r_uid=uid,r_content=r_content)
        # 处理图片

        r_id = str(obj.id)
        r_photo = request.FILES.get('r_photo')
        r_photo_path= ''
        if r_photo:
            # 保存图片路径
            r_photo_path = 'static/img/r_photo/'+r_id+'_'+r_photo.name
            import os
            file = open(os.path.join(r_photo_path),'wb')
            for line in r_photo.chunks():
                file.write(line)
            file.close()
        #     将数据添加到留言表
        models.Reply.objects.filter(id=r_id).update(r_photo='/' + r_photo_path)
        return redirect('/single/' + str(tid))

# 公告页面
def single_an(request,aid):
    if request.method == 'GET':
        try:
            an = models.Announcement.objects.get(id=aid)
        except Exception as e:
            return redirect('/home')
        response = {
            'id':aid,
            'a_title':an.a_title,
            'a_content':an.a_content,
            'a_url':an.a_url,
            # 对象解构
            **get_common_data(request)
        }
        return render(request,'single-an.html',response)


# 所有帖子
def all_tie(request, kid, reply_limit, time_limit):
    if request.method == 'GET':
        topics = models.Topic.objects.filter().order_by('-id')
        # 搜索接收一个字段，查询标题或者简介里有关键字的帖子
        keys = request.GET.get('keys', '')
        if keys != '':
            # 按关键字查询标题里含有关键字的
            topics = topics.filter(t_title__icontains=keys)
        if kid != '0' or reply_limit != '0' or time_limit != '0':
            # 筛选分类
            if kid != '0':
                topics = topics.filter(t_kind=kid)
            # 筛选回复数量
            tmp = []
            for topic in topics:
                # 查看每个帖子的回复数量
                count = len(models.Reply.objects.filter(r_tid=topic.id))
                # print(count)
                print(reply_limit)
                if reply_limit == '0':
                    pass
                elif reply_limit == '1':  # 1是大于100
                    print('到1了')
                    if count < 100:
                        print('到了')
                        continue
                elif reply_limit == '2':  # 2是30-100
                    if count < 30 or count > 100:
                        continue
                elif reply_limit == '3':  # 3是小于30
                    if count > 30:
                        continue
                tmp.append(topic)
            topics = tmp

            # 筛选发布时间
            tmp = []
            for topic in topics:
                if time_limit == '0':  # 0是全部时间
                    pass
                elif time_limit == '1':  # 1是1个月内
                    # 如果在限制之前，就筛掉
                    pass
                elif time_limit == '2':  # 2是3个月内
                    # 如果在限制之前，就筛掉
                    pass
                elif time_limit == '3':  # 3是6个月内
                    # 如果在限制之前，就筛掉
                    pass
                elif time_limit == '4':  # 4是1年内
                    # 如果在限制之前，就筛掉
                    pass
                tmp.append(topic)
            topics = tmp

        response = {
            'topics': topics,
            'kid': kid,
            'time_limit': time_limit,
            'reply_limit': reply_limit,
            'search_url':'/all-'+kid+'-'+reply_limit+'-'+time_limit,
            'keys':keys,
            # 对象解构
            **get_common_data(request)
        }
        return render(request, 'all.html', response)

# 上传图片
def upload_img(request):
    if request.method == 'POST':
        photo = request.FILES.get('photo')
        if photo:
            # 保存图片路径
            photo_path = 'static/img/r_photo/upload/' + str(uuid.uuid1()) + '_' + photo.name
            import os
            file = open(os.path.join(photo_path), 'wb')
            for line in photo.chunks():
                file.write(line)
            file.close()
            return HttpResponse(json.dumps({
                "errno": 0,
                "data": ['/' + photo_path]
            }))