from django.contrib import admin
from django.urls import path,re_path
from App import views
from App.my_admin import views as admin_views
import re

urlpatterns = [

    path('', views.home, name='home'),
    # 登陆的路由
    path('login/', views.login,name='login'),
#     登陆和注册成功之后的返回的路由
    path('home/',views.home,name='home'),
    path('edit_pwd/',views.edit_pwd,name='edit-pwd'),
    path('publish/',views.publish,name='publish'),
#     之前全部都是没有参数的路由，没有使用正则的路由
#     但参数的路由  显示单个帖子
    path('single/<int:tid>/',views.single,name='single'),
    path('single-an-<int:aid>/',views.single_an,name='single_an'),

    # 管理员页面url
    # 管理员登录页
    path('my_admin/',admin_views.admin,name='admin'),
    # 管理员首页
    path('admin_home/',admin_views.admin_home,name='admin_home'),
    # 帖子管理
    path('admin_home/topic_manage/',admin_views.topic_manage,name='topic_manage'),
    # 公告管理
    path('admin_home/announcement/',admin_views.announcement,name='announcement'),
    # 板块管理
    path('admin_home/kind_manage/',admin_views.kind_manage,name='kind_manage'),
    # 留言管理 TODO



    # 查看全部帖子的方法
    # path('all-<int:kid>-<int:reply_limit>-<int:time_limit>',views.all_tie,name='all_tie'),
#     2.XX之后  path和re_path 结合使用  url
    re_path(r'^all-(?P<kid>\d+)-(?P<reply_limit>\d+)-(?P<time_limit>\d+)',views.all_tie,name='all_tie'),

    # 上传图片
    path('upload_img/',views.upload_img,name='upload_img'),

]
