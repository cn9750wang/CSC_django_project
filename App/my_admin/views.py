import json

from django.http import HttpResponse
from django.shortcuts import render,redirect
# Create your views here.

# 登陆
from App import models

# 管理员登录检查装饰器
def is_admin(callback):
    def check_admin(request,*callback_args, **callback_kwargs):
        # 判断是不是管理登陆
        if not request.session.get('admin_uid'):
            return redirect('/my_admin')
        return callback(request,*callback_args,**callback_kwargs)
        pass
    return check_admin

# 管理员登陆   专门一个账号  自己加表
def admin(request):
    if request.method == 'GET':
        return render(request,'admin/admin.html')
    elif request.method =='POST':
        admin_uid = request.POST.get('admin_id')
        admin_pwd = request.POST.get('admin_pwd')

        response = {'msg':'','status':False}
        # 唯一管理员账号
        if admin_uid == 'guanliyuan' and admin_pwd=='123456':
            response['status'] = True
            request.session['admin_uid'] = 'guanliyuan'
            return HttpResponse(json.dumps(response))
        else:
            response['msg'] = '用户名或者密码错误'
            return HttpResponse(json.dumps(response))


# 管理员首页
@is_admin
def admin_home(request):
    return render(request, 'admin/admin-home.html')

# 公告管理
@is_admin
def announcement(request):
    # 查询公告
    if request.method == 'GET':
        announcements= models.Announcement.objects.filter()
        response = {'announcements':announcements}
        return render(request,'admin/announcement.html',response)

    #删除  添加公告
    elif request.method == 'POST':
#         接收页面的请求类型
        p_type = request.POST.get('type')
        response = {'msg':'','status':False}
        if p_type == 'delete':
            a_id = request.POST.get('a_id')
            models.Announcement.objects.filter(id=a_id).delete()
            response['status'] = True
        elif p_type=='create':
#             添加一条公告
            a_title = request.POST.get('a_title')
            a_content = request.POST.get('a_content')
            a_url = request.POST.get('a_url')
            models.Announcement.objects.create(a_title=a_title,a_content=a_content,a_url=a_url)
            response['status'] = True
        return HttpResponse(json.dumps(response))


# 板块管理
@is_admin
def kind_manage(request):
    # 查询板块
    if request.method == 'GET':
        kinds = models.Kind.objects.filter()
        response = {'kinds': kinds}
        return render(request, 'admin/kind-manage.html', response)

    #     删除  添加  类别
    elif request.method == 'POST':
        #         接收页面的请求类型
        p_type = request.POST.get('type')
        response = {'msg': '', 'status': False}
        if p_type == 'delete':
            k_id = request.POST.get('k_id')
            models.Kind.objects.filter(id=k_id).delete()
            response['status'] = True
        elif p_type == 'create':
            #             添加一个类别
            k_name = request.POST.get('k_name')
            models.Kind.objects.create(k_name=k_name)
            response['status'] = True
        return HttpResponse(json.dumps(response))



# 帖子管理
@is_admin
def topic_manage(request):
    if request.method == 'GET':
        topics = models.Topic.objects.filter()
        response = {'topics':topics}
        return render(request,'admin/topic-manage.html',response)

#     删除帖子  推荐帖子  取消推荐
    elif request.method == 'POST':
        p_type = request.POST.get('type')
        response={'msg':'','status':False}
        print(p_type)

#       实现删除
        if p_type == 'delete':
            t_id = request.POST.get('t_id')
            models.Topic.objects.filter(id=t_id).delete()
            response['status'] = True
        #     推荐
        if p_type == 'tuijian':
            t_id = request.POST.get('t_id')
            models.Topic.objects.filter(id=t_id).update(recommend=True)
            response['status'] = True
        #     取消推荐
        if p_type == 'qxtuijian':
            t_id = request.POST.get('t_id')
            models.Topic.objects.filter(id=t_id).update(recommend=False)
            response['status'] = True
        return HttpResponse(json.dumps(response))
