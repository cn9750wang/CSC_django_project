from django.db import models

# Create your models here.

# 用户表
class User(models.Model):
    # id系统自动会加载 统一叫id
    uid = models.CharField(verbose_name='电话/用户号',max_length=16,unique=True)
    password = models.CharField(verbose_name='密码',max_length=16)
    create_time = models.DateField(verbose_name='创建时间',auto_now_add=True)

    class Meta:
        db_table = 'user'

        # 模板的使用  遵循模板语言的特性  但是基本的前端页面都可以去访问
# 帖子表  需要哪些字段组合
"""
帖子：
    帖子所属的用户id
    类型
    图片
    创建的时间（更新时间）
    正文
    标题
    简介 
    是否推荐
"""
class Topic(models.Model):
    t_uid = models.CharField(verbose_name='帖子所属的用户的id',max_length=16)
    t_kind = models.CharField(verbose_name='类别',max_length=32)
    t_photo = models.CharField(verbose_name='帖子图片',max_length=128,null=True)
    create_time=models.DateField(verbose_name='创建时间',auto_now_add=True)

    t_content=models.TextField(verbose_name='帖子正文')
    t_title = models.CharField(verbose_name='帖子标题',max_length=64)
    t_introduce = models.CharField(verbose_name='帖子简介',max_length=255)
    recommend = models.BooleanField(verbose_name='是否推荐',default=False)

    class Meta:
        db_table = 'topic'
#     分类
class Kind(models.Model):
#     系统会主动给一个主键id  自增
#     类别名字
    k_name = models.CharField(verbose_name='类别',max_length=16,unique=True)
    class Meta:
        db_table='kind'

#     公告表
class Announcement(models.Model):
    a_title = models.CharField(verbose_name='公告标题',max_length=64)
    a_content = models.CharField(verbose_name='公告内容',max_length=255,null=True)
    a_url = models.CharField(verbose_name='外部链接',max_length=255,null=True)
    class Meta:
        db_table = 'announcement'

# 留言表
"""
id---
帖子id
发表者的用户名
留言的内容
留言的时间
留言的图片

"""
class Reply(models.Model):
    # 没有外键的关系  联合查询
    r_tid =models.CharField(verbose_name='帖子id',max_length=16)
    r_uid=models.CharField(verbose_name='留言者id',max_length=16)
    r_content = models.CharField(verbose_name='留言内容',max_length=255)
    r_photo =  models.CharField(verbose_name='留言图片',max_length=128,null=True)
    r_time = models.DateField(verbose_name='留言时间',auto_now_add=True)

    class Meta:
        db_table='reply'